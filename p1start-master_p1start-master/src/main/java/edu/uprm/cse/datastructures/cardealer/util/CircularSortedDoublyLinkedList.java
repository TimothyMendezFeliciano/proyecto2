package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	private DNode<E> head;
	private Comparator<E> cmp; // variable to assist comparation
	private int size;
	
	//Nested Class for DNode
	private static class DNode<E> {

		private E element;
		private DNode<E> next;
		private DNode<E> prev;

		public DNode(DNode<E> p, E e, DNode<E> n) {
			element = e;
			next = n;
			prev = p;
		}
		public DNode(E e) {
			element = e;
			next = null;
			prev = null;
		}
		public DNode() {
			element = null;
			prev = null;
			next = null;
		}
		public E getElement() { return element;}
		public DNode<E> getNext() { return next;}
		public DNode<E> getPrev() { return prev;}
		public void setNext(DNode<E> n) { next = n;}
		public void setPrev(DNode<E> p) {prev = p;}
		public void setElement(E e) { element = e;}
	}

	// Default Comparator in case no Comparator is entered
	protected static class DefaultComparator<E> implements Comparator<E> {
		public int compare(E o1, E o2) {
			try { 
				return ((Comparable<E>) o1).compareTo(o2); 
			} catch (ClassCastException e) { 
				throw new IllegalArgumentException("Instantiated data type must be Comparable");
			}
		} 
	}

	public CircularSortedDoublyLinkedList(Comparator<E> cmp) {
		head = new DNode<E>();	// head must first be initialized
		head.setNext(head);		// in order to set itself as next and prev
		head.setPrev(head);
		size = 0;
		this.cmp = cmp;
	}
	public CircularSortedDoublyLinkedList() {
		head = new DNode<E>();	// head must first be initialized
		head.setNext(head);		// in order to set itself as next and prev
		head.setPrev(head);
		size = 0;
		cmp = new DefaultComparator<E>();
	}

	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(E obj) {
		// TODO Auto-generated method stub
		DNode<E> current = head.getNext();//Node to assist iteration through the LinkedList

		while (current != head && cmp.compare(obj, current.getElement()) > 0) 
			current = current.getNext(); 
		//end while loop

		DNode<E> newNode = new DNode<E>(current.getPrev(), obj, current);
		current.getPrev().setNext(newNode);
		current.setPrev(newNode);
		size++;
		return true;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	private DNode<E> cleanLinks(DNode<E> obj) { //disconnects node from the list, and adjust size

		
		//Obtain previous and next nodes to obj
		DNode<E> nextNode = obj.getNext();
		DNode<E> prevNode = obj.getPrev();
		
		obj.setNext(null); // Assist Java Garbage Collector
		obj.setPrev(null);
		nextNode.setPrev(prevNode);
		prevNode.setNext(nextNode);
		size--;
		return nextNode; // Node is return to assist removeAll method
	}

	@Override
	public boolean remove(E obj) {
		// TODO Auto-generated method stub

		DNode<E> current = head.getNext();//Node to assist iteration through the LinkedList
		
		//If list is empty return false
		while(current != head) {
			// inspects if a an element is equal to argument obj
			if(cmp.compare(obj, current.getElement()) == 0) {
				cleanLinks(current);
				return true;
			}
			current = current.getNext();
		}

		return false;
	}

	@Override
	public boolean remove(int index) {
		// TODO Auto-generated method stub
		if(index < 0 || index > size) {
			return false;
		}
		int counter = 0;
		DNode<E> current = head.getNext();//Node to assist iteration through the LinkedList

		while(current != head) { // traverses the LinkedList until head is reached
			if(counter == index) {
				cleanLinks(current);

				return true;
			}

			counter++;
			current = current.getNext();

		}
		return false;
	}

	@Override
	public int removeAll(E obj) {
		// TODO Auto-generated method stub
		DNode<E> current = head.getNext();//Node to assist iteration through the LinkedList
		int counter =0;

		while(current != head) {
			if(cmp.compare(current.getElement(), obj) == 0) { // inspects if a an element is equal to argument e
				current = cleanLinks(current);
				counter++;
			}else {
				current = current.getNext();
			}
		}

		return counter;
	}

	@Override
	public E first() {
		// TODO Auto-generated method stub
		return this.isEmpty() ? null : head.getNext().getElement();
	}

	@Override
	public E last() {
		// TODO Auto-generated method stub
		return this.isEmpty() ? null : head.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		// TODO Auto-generated method stub

		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException("Index is not within the size");
		}

		int counter = 0;
		DNode<E> current = head.getNext();//Node to assist iteration through the LinkedList

		while(current != head && counter != index) { // traverses the LinkedList until head is reached
			counter++;
			current = current.getNext();
		}

		return current.getElement();
	}

	@Override
	public void clear() { // Delete the list
		// TODO Auto-generated method stub
		DNode<E> current = head.getNext();//Node to assist iteration through the LinkedList
		while(current != head) {
			current = cleanLinks(current);
		}
		head.setNext(head);
		head.setPrev(head);
	}

	@Override
	public boolean contains(E e) {
		// TODO Auto-generated method stub

		DNode<E> current = head.getNext();//Node to assist iteration through the LinkedList

		while(current != head) { // traverses the LinkedList until head is reached
			if(cmp.compare(current.getElement(), e) == 0) { // inspects if a an element is equal to argument e
				return true; // return true 
			}

			current = current.getNext();
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (size==0);
	}

	@Override
	public int firstIndex(E e) {
		// TODO Auto-generated method stub
		int counter = 0;
		DNode<E> current = head.getNext();//Node to assist iteration through the LinkedList

		while(current != head) { // traverses the LinkedList until head is reached
			if(cmp.compare(current.getElement(), e) == 0) {

				return counter; // return index where e first appears
			}

			counter++; // change "index" of list
			current = current.getNext();

		}

		return -1; // Return -1 if e never in list
	}

	@Override
	public int lastIndex(E e) {
		int counter = size-1;
		DNode<E> current = head.getPrev(); // assistive node to begin with node after dummy head

		while(current != head) { // traverses the LinkedList until head is reached
			if(cmp.compare(current.getElement(), e) == 0) {

				return counter; // return index where e first appears
			}

			counter--; // change "index" of list
			current = current.getPrev();

		}

		return -1; // Return -1 if e never in list
	}

	public Object[] toArray() {
		Object[] list = new Object[size];

		DNode<E> current = head.getNext();//Node to assist iteration through the LinkedList

		if(isEmpty()) return list;

		for(int i = 0; i < size;i++) {
			list[i] = current.getElement();
			current = current.getNext();
		}
		return list;
	}

}
