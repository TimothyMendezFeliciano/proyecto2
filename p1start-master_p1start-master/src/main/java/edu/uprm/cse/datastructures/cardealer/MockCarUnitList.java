package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.CarUnit;
import edu.uprm.cse.datastructures.cardealer.model.CarUnitComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class MockCarUnitList {
	
	private static final CircularSortedDoublyLinkedList<CarUnit> cuList = new CircularSortedDoublyLinkedList<CarUnit>(new CarUnitComparator());
	  
	  public static CircularSortedDoublyLinkedList<CarUnit> getInstance(){
	    return cuList;
	  }
}
