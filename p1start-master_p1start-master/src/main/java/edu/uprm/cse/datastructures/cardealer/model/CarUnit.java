package edu.uprm.cse.datastructures.cardealer.model;

public class CarUnit {
	
	private long carUnitId; // internal id of the unit
	private long carId; // id of the car object that represents the general for the car. 
                                           // This Car from project 1.
	private String vin; // vehicle identification number
	private String color; // car color
	private String carPlate; // car plate (null until sold)
	private long personId; // id of the person who purchased the car. (null until 
                                                  //purchased)	
	public long getCarUnitId() {
		return carUnitId;
	}
	public long getCarId() {
		return carId;
	}
	public String getVIN() {
		return vin;
	}
	public String getColor() {
		return color;
	}
	public String getCarPlate() {
		return carPlate;
	}
	public long getPersonId() {
		return personId;
	}
	public CarUnit() {
		
	}
	public CarUnit(long carUnitId, long carId, String vin, String color, String carPlate, long personId) {
		super();
		this.carUnitId = carUnitId;
		this.carId = carId;
		this.vin = vin;
		this.color = color;
		this.carPlate = carPlate;
		this.personId = personId;
	}
	@Override
	public String toString() {
		return "CarUnit [carUnitId=" + carUnitId + ", carId=" + carId + ", VIN=" + vin + ", color=" + color
				+ ", carPlate=" + carPlate + ", personId=" + personId + "]";
	}


}
