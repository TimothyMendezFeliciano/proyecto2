package edu.uprm.cse.datastructures.cardealer.model;

public class Appointment {
	
	private long appointmentId; // internal id of the appointment
	private long carUnitId; // id of the car to be serviced
	private String job; // description of the job to be done (i.e.: “oil change”)
	private double bill; // cost of the service (initially 0).
	
	public long getAppointmentId() {
		return appointmentId;
	}
	public long getCarUnitId() {
		return carUnitId;
	}
	public String getJob() {
		return job;
	}
	public double getBill() {
		return bill;
	}
	public Appointment() {
		// TODO Auto-generated constructor stub
	}
	public Appointment(long appointmentId, long carUnitId, String job, double bill) {
		super();
		this.appointmentId = appointmentId;
		this.carUnitId = carUnitId;
		this.job = job;
		this.bill = bill;
	}
	@Override
	public String toString() {
		return "Appointment [appointmentId=" + appointmentId + ", carUnitId=" + carUnitId + ", job=" + job + ", bill="
				+ bill + "]";
	}


}
