package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{
	// Comparate me, Senpai
	
	
	/** Comparation to be done by Strings of brand, model
		 and options to decide which should go first.
		 @param a, and b are both of type car.
		@return 0 if they are equal, -1 if a goes first.
		1 if a goes after b
		*/
	@Override
	public int compare(Car a, Car b) {
		// TODO Auto-generated method stub
		
		int cmpBrand = a.getCarBrand().compareTo(b.getCarBrand());
		int cmpModel = a.getCarModel().compareTo(b.getCarModel());
		int cmpOption = a.getCarModelOption().compareTo(b.getCarModelOption());
		
		if (cmpBrand != 0) return cmpBrand; 
		if (cmpModel != 0) return cmpModel; 
		return cmpOption; 
	}
}
