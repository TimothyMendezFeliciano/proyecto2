package edu.uprm.cse.datastructures.cardealer.model;

public class Person {
	private long personId; // internal id of the person
	private String firstName; // first name
	private String lastName; // lastname
	private Integer age; // age
	private Character gender; // gender
	private String phone; // phone number
	
	public long getPersonId() {
		return personId;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public Integer getAge() {
		return age;
	}
	public Character getGender() {
		return gender;
	}
	public String getPhone() {
		return phone;
	}
	
	public Person() {
		
	}
	public Person(long personId, String firstName, String lastName, Integer age, Character gender, String phone) {
		super();
		this.personId = personId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.gender = gender;
		this.phone = phone;
	}
	
	
	@Override
	public String toString() {
		return "Car [personId=" + personId + ", firstName=" + firstName + ", lasName=" + lastName + ", age="
				+ age + ", gender=" + gender + "phone=" + phone + "]";
	}

}
