package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.model.PersonComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class MockPersonList {
	
	private static final CircularSortedDoublyLinkedList<Person> pList = new CircularSortedDoublyLinkedList<Person>(new PersonComparator());
	
	public static CircularSortedDoublyLinkedList<Person> getInstance(){
		return pList;
	}
}
