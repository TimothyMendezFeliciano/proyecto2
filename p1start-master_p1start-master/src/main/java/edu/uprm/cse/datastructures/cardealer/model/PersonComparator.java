package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class PersonComparator implements Comparator<Person> {

	@Override
	public int compare(Person p1, Person p2) {
		// TODO Auto-generated method stub
		int lastName = p1.getLastName().compareTo(p2.getLastName());
		int firstName = p1.getFirstName().compareTo(p2.getFirstName());
		
		if(lastName != 0) return lastName;
		return firstName;
	}
	
}
