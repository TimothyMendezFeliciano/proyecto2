package edu.uprm.cse.datastructures.cardealer;

import java.util.ArrayList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars") // Esta clase se dedica a procesar el JSON
public class CarManager {
	
	private static CircularSortedDoublyLinkedList<Car>  carList = new MockDealerList().getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		
		Car[] result = new Car[carList.size()];
		
		for(int i = 0; i < carList.size(); i++) {
			result[i] = carList.get(i);
		}

		return result;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {

		for(int i = 0 ; i < carList.size(); i++) {
			if(id == carList.get(i).getCarId()) {
				return carList.get(i);
			}
		}
		throw new NotFoundException(new JsonError("Error", "Customer " + id + "not found"));
	}// end of get by id
	
	@GET
	@Path("/year/{year}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getYear(@PathParam("year") int year) {
		Car[] result = new Car[carList.size()];
		int index = 0;
		for(int i = 0 ; i < carList.size(); i++) {
			if(year == carList.get(i).getCarYear()) {
				result[index++] = carList.get(i);
			}
		}
		if(result.length == 0) {
			throw new NotFoundException(new JsonError("Error", "Customer " + year + "not found"));
		}
		else {
			return result;
		}
	}
	
	@GET
	@Path("/brand/{brand}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getBrand(@PathParam("brand") String brand) {
		Car[] result = new Car[carList.size()];
		int index = 0;
		
		for(int i = 0 ; i < carList.size(); i++) {
			if(brand.equals(carList.get(i).getCarBrand())) {
				result[index++] = carList.get(i);
			}
		}
		if(result.length == 0) {
			throw new NotFoundException(new JsonError("Error", "Customer " + brand + "not found"));
		}
		else {
			return result;
		}
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {

		carList.add(car);
		return Response.status(201).build();
	}
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		for(int i = 0; i < carList.size(); i++) {
			if(car.getCarId() == carList.get(i).getCarId()) {
				carList.remove(i);
				carList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {

		for(int i = 0; i < carList.size(); i++) {
			if(id == carList.get(i).getCarId()) {
				carList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
