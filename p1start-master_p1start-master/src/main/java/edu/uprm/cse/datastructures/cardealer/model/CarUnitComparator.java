package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarUnitComparator implements Comparator<CarUnit>{

	@Override
	public int compare(CarUnit cu1, CarUnit cu2) {
		// TODO Auto-generated method stub
		return cu1.getVIN().compareTo(cu2.getVIN());
	}

}
