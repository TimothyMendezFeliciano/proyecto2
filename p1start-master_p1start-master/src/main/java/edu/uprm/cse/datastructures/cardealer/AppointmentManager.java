package edu.uprm.cse.datastructures.cardealer;

import java.util.ArrayList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Appointment;
import edu.uprm.cse.datastructures.cardealer.util.LinkedPositionalList;
import interfaces.Position;
import interfaces.PositionalList;

@Path("/appointment") // Esta clase se dedica a procesar el JSON

public class AppointmentManager {

	private static PositionalList<Appointment>[] appList = new LinkedPositionalList[5];

	private static PositionalList<Appointment> monday = new LinkedPositionalList<>();
	private static PositionalList<Appointment> tuesday = new LinkedPositionalList<>();
	private static PositionalList<Appointment> wednesday = new LinkedPositionalList<>();
	private static PositionalList<Appointment> thursday = new LinkedPositionalList<>();
	private static PositionalList<Appointment> friday = new LinkedPositionalList<>();

	public AppointmentManager() {

		appList[0] = monday;
		appList[1] = tuesday;
		appList[2] = wednesday;
		appList[3] = thursday;
		appList[4] = friday;
		//index to be determined by day
		// 0 is Monday
		// 1 is Tuesday
		// 2 is Wednesday
		// 3 is Thursday
		// 4 is Friday
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Appointment[] getAllAppointments() {
		int SIZE = appList[0].size() + appList[1].size() + appList[2].size() + appList[3].size() + appList[4].size();
		Appointment[] result = new Appointment[SIZE];
		int index = 0;
		for(PositionalList<Appointment> p: appList) {
			for(Appointment a: p) {
				result[index++] = a;
			}
		}
		return result;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Appointment getAppointment(@PathParam("id")long id) {
		for(PositionalList<Appointment> p: appList) {
			for(Appointment a: p) {
				if(a.getAppointmentId() == id) {
					return a;
				}
			}
		}
		throw new NotFoundException(new JsonError("Error", "Customer " + id + "not found"));
	}// end of get by id

	@GET
	@Path("/day/{day}")
	@Produces(MediaType.APPLICATION_JSON)
	public Appointment[] getAppointmentbyDay(@PathParam("day") int day) {

		Appointment[] result = new Appointment[appList[day].size()]; //Set result size to size of specified day array

		if(day < 0 || day > 4) return result;

		int index = 0;
		
		for(Appointment a: appList[day]) {
			result[index++] = a; // Add all the correct appointments
		}
		return result;

	}

	@POST
	@Path("/add/{day}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addAppointment(@PathParam("day") int day, Appointment app) {
		if(day < 0 || day > 4) return Response.status(404).build();
		appList[day].addLast(app);
		return Response.status(201).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateAppointment(Appointment appointment) {
		for(PositionalList<Appointment> p: appList) {
			for(Position<Appointment> a: p.positions()) {
				//
				if(a.getElement().getAppointmentId() == appointment.getAppointmentId()) {
					p.remove(a);
					p.addLast(appointment);
					return Response.status(Response.Status.OK).build();
				}
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteAppointment(@PathParam("id")long id) {
		for(PositionalList<Appointment> p: appList) {
			for(Position<Appointment> a: p.positions()) {
				if(a.getElement().getAppointmentId() == id) {
					p.remove(a);
					return Response.status(Response.Status.OK).build();
				}
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}