 package edu.uprm.cse.datastructures.cardealer;

import java.util.ArrayList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/person") // Esta clase se dedica a procesar el JSON

public class PersonManager {

	private static CircularSortedDoublyLinkedList<Person>  personList = new MockPersonList().getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Person[] getAllPerson() {
		
		Person[] result= new Person[personList.size()];
		
		for(int i = 0; i < personList.size(); i++) {
			result[i] = personList.get(i);
		}

		return result;
	}// end of get

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Person getPerson(@PathParam("id") long id) {

		for(int i = 0 ; i < personList.size(); i++) {
			if(id == personList.get(i).getPersonId()) {
				return personList.get(i);
			}
		}
		throw new NotFoundException(new JsonError("Error", "Customer " + id + "not found"));
	} // end of id

	@GET
	@Path("/lastname/{lastname}")
	@Produces(MediaType.APPLICATION_JSON)
	public Person[] getLastName(@PathParam("lastname") String lastname) {
		Person[] result = new Person[personList.size()];
		int index = 0;

		for(int i = 0 ; i < personList.size(); i++) {
			if(lastname.equals(personList.get(i).getLastName())) {
				result[index++] = personList.get(i);
			}
		}
		if(result.length == 0) {
			throw new NotFoundException(new JsonError("Error", "Customer " + lastname + "not found"));
		}
		else {
			return result;
		}
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPerson(Person person) {

		personList.add(person);
		return Response.status(201).build();
	}// end of add

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePerson(Person person) {
		for(int i = 0; i < personList.size(); i++) {
			if(person.getPersonId() == personList.get(i).getPersonId()) {
				personList.remove(i);
				personList.add(person);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}// end of update

	@DELETE
	@Path("/{id}/delete")
	public Response deletePerson(@PathParam("id") long id) {

		for(int i = 0; i < personList.size(); i++) {
			if(id == personList.get(i).getPersonId()) {
				personList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
