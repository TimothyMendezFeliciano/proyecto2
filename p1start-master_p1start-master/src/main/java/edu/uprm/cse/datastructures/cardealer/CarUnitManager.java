package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.CarUnit;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/carunit")
public class CarUnitManager {
	
	private static CircularSortedDoublyLinkedList<CarUnit>  cuList = new MockCarUnitList().getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public CarUnit[] getAllPerson() {
		
		CarUnit[] result = new CarUnit[cuList.size()];
		
		for(int i = 0; i < cuList.size(); i++) {
			result[i] = cuList.get(i);
		}

		return result;
	}// end of get
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public CarUnit getCarUnit(@PathParam("id") long id) {

		for(int i = 0 ; i < cuList.size(); i++) {
			if(id == cuList.get(i).getCarUnitId()) {
				return cuList.get(i);
			}
		}
		throw new NotFoundException(new JsonError("Error", "Customer " + id + "not found"));
	} // end of id
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCarUnit(CarUnit cu) {

		cuList.add(cu);
		return Response.status(201).build();
	}// end of add
	
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCarUnit(CarUnit cu) {
		for(int i = 0; i < cuList.size(); i++) {
			if(cu.getCarUnitId() == cuList.get(i).getCarUnitId()) {
				cuList.remove(i);
				cuList.add(cu);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}// end of update
	
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCarUnit(@PathParam("id") long id) {

		for(int i = 0; i < cuList.size(); i++) {
			if(id == cuList.get(i).getCarUnitId()) {
				cuList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
