package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Appointment;
import edu.uprm.cse.datastructures.cardealer.util.LinkedPositionalList;
import interfaces.Position;
import interfaces.PositionalList;

public class AppointmentTester {
	private static LinkedPositionalList<Appointment>[] appList = new LinkedPositionalList[4];
	
	private Appointment senpai = new Appointment(0, 1, "aceite", 44.0);
	private Appointment nani = new Appointment(1, 2, "aros", 197.0);
	private Appointment subNani = new Appointment(2, 3, "lavado", 24.0);
	private Appointment ohegai = new Appointment(2, 1, "aceite", 44.0);

	public AppointmentTester() {
		for(int i =0; i <= 3; i++) {
			appList[i] = new LinkedPositionalList<Appointment>();
		}
	appList[0].addLast(senpai);
	appList[1].addLast(nani);
	Position<Appointment> este = appList[1].addLast(subNani);
	appList[2].addLast(ohegai);
	
	appList[1].remove(este);
	}
	
	public void view() {
		for(int i = 0; i < appList.length; i++) {
			for(Position<Appointment> c : appList[i].positions() ) {
				System.out.println(c.getElement().toString() );
			}
		}
	}
	

}
